export const environment = {
  production: true,
  //apiUrl: 'https://localhost:44365/api/',
  // apiUrl: 'https://www.appenza.app:5053/api/',
  apiUrl: '',


  //testing
  firebaseConfig: {
    apiKey: 'AIzaSyDlpbCp2wEe1PwMwMl4GyphAuZeF4WW8ew',
    authDomain: 'extra-382bc.firebaseapp.com',
    databaseURL: 'https://extra-382bc-default-rtdb.firebaseio.com',
    storageBucket: 'extra-382bc.appspot.com',
  },

  //login
  SSOLogin: "Account/MoELogin",
  LoginUser: "Account/Login",
  Register: "Account/Register",
  CompleteProfile: "Account/CompleteProfile",
  GetProfile: "Account/GetInfo",
  UpdateProfile: "Account/UpdateInfo",
  GetMySubjects: "Account/GetMySubjects",
  SendResetEmail: "Account/SendResetPasswordEmail",
  ResetPassword: "Account/ResetPassword",
  //lookup
  GetGovernorates: "lookup/GetAllGovernorates",
  GetDistrictsByGovId: "lookup/GetDistrictsByGovId",
  GetSchoolsByDisId: "lookup/GetSchoolsByDistrictId",
  GetRegistrationAttachments: "lookup/GetRegistrationAttachments",
  GetApplicationAttachments: "lookup/GetApplicationAttachments",
  GetAllSubjects: "lookup/GetAllSubjects",
  GetsubjectsbyGrades: "lookup/GetsubjectsbyGrades",
  GetAllStagesWithGrades: "lookup/GetAllStagesWithGrades",

  // Sessions
  GetTeacherComingSessions: "Sessions/GetTeacherComingSessions",
  GetTeacherPreviousSessions: "Sessions/GetTeacherPreviousSessions",
  GetSessionAttendance: "Sessions/GetSessionAttendance",
  AttendOfflineStudent: "Sessions/AttendOfflineStudent",
  TakeStudentsAbsence: "Sessions/TakeStudentsAbsence",

  //Dashboard
  GetHomeStatus: "Dashboard/Status",

  //Applications
  GetApplications: "Application/GetMyApplications",
  AddApplication: "Application/Create",

}
