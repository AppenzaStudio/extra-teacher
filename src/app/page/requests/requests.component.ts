import { DomSanitizer } from '@angular/platform-browser';
import { FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { ApplicationsServiceService } from 'app/shared/services/applicationsService/applications-service.service';
import { HelperService } from 'app/shared/services/helper.service';
import { LookupService } from 'app/shared/services/lookupServices/lookup.service';
import { AuthUserService } from 'app/shared/services/userServices/auth-user.service';
import { IResponse } from 'app/shared/interfaces/response.interface';
import * as moment from 'moment';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {
  requests;
  comment;
  moment = moment;

  governorateList: any[] = [];
  districtList: any[] = [];
  schoolsList: any[] = [];
  subjectList: any[] = [];
  selected = new FormControl(0);
  govId = null;
  disId = null;
  schoolId = null;
  subjectId = null;
  attachments: any = [];
  attachmentList: any = [];
  attachmentsInvalid = true;
  uploadedFile: any;
  public fileData: any = null;
  fileChanged = false;
  urls = [];

  constructor(private modalService: NgbModal
    , private applicationsServiceService: ApplicationsServiceService,
    private helper: HelperService,
    private sanitizer: DomSanitizer,
    private lookupService: LookupService,
    public auth: AuthUserService,
  ) { }

  ngOnInit(): void {
    this.GetApplications();
    this.getAllGovernorates();
    this.getAllSubjects();
  }

  openModal(modalName, size, comment? ,backdrop?) {
    this.comment = comment;
    this.modalService.open(
      modalName,
      {
        windowClass: 'modal-holder ',
        backdropClass: 'light-blue-backdrop',
        centered: true, keyboard: false,
        backdrop: backdrop ? 'static' : null,
        size: size
      });
  }

  async GetApplications() {
    try {
      const res: any = await this.applicationsServiceService.GetApplications();
      if (res.statusCode.toString().startsWith('2')) {
        this.requests = res.returnObject
      } else {
        this.helper.alertError(res.arabicMessage);
      }
    } catch (err) {
      console.log(err);
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
      
    }
  }

  async AddApplication() {
    try {
      let body =
      {
        SchoolId: 0,
        SubjectId: 0
      }
      body.SchoolId = +this.schoolId;
      body.SubjectId = +this.subjectId;

      let formData = new FormData();
      formData = this.helper.jsonToFormData(body);
      for (let index = 0; index < this.attachmentList.length; index++) {
        formData.append(`Attachements[${index}].Id`, this.attachmentList[index].Id);
        formData.append(`Attachements[${index}].Src`, this.attachmentList[index].Src);
      }
      const res: any = await this.applicationsServiceService.AddApplications(formData);
      if (res && res.success) {
         this.reset();
         this.selected.setValue(0);
         this.GetApplications();
         this.modalService.dismissAll();
         this.helper.alertSuccess(res.arabicMessage);
      } else {
        this.helper.alertError(res.arabicMessage);
      }
    } catch (err) {
      console.log(err)
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }
  reset(){
    this.schoolId = null;
    this.subjectId = null;
    this.govId = null;
    this.disId = null;
    this.attachments = [];
    this.attachmentList = [];
  }

  async getAllGovernorates() {
    try {
      const res: IResponse = await this.lookupService.getAllGovernorates();
      if (res && res.success) {
        this.governorateList = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      console.log(err);
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }
  async getAlldistricts() {
    this.districtList = [];
    try {
      const res: IResponse = await this.lookupService.getDistrictsByGovId(+this.govId);
      if (res && res.success) {
        this.districtList = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      console.log(err)
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }

  async getAllschools() {
    this.schoolsList = [];
    try {
      const res: IResponse = await this.lookupService.getSchoolsByDisId(+this.disId);
      if (res && res.success) {
        this.schoolsList = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      console.log(err)
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }
  async getAllSubjects() {
    try {
      const res: IResponse = await this.auth.getMySubjects();
      if (res && res.success) {
        this.subjectList = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }

  getIndex(index) {
    if (index == 1 && this.attachments.length == 0) {
      this.getAttachmentTypes();
    }
    if (index == 0 && (this.governorateList.length == 0 || this.subjectList.length == 0)) {
      this.getAllSubjects();
      this.getAllGovernorates();
    }
  }
  async getAttachmentTypes() {
    this.districtList = [];
    try {
      const res: IResponse = await this.lookupService.getApplicationAttachments();
      if (res && res.success) {
        this.attachments = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }
  // upload
  uploadFile($event, index, size?) {

    this.fileData = $event.target.files[0];
    if ((this.fileData.size / 1024) <= size) {
      this.fileChanged = true;
      this.uploadedFile = true;
      this.attachments[index].url = this.sanitizer.bypassSecurityTrustUrl(
        URL.createObjectURL(this.fileData)
      );
      this.attachments[index].src = this.fileData;
      console.log(this.fileData);
    } else {
      this.helper.alertError(`
     هذا الملف يجيب ان يكون اقل من
     ${(size / 1024).toFixed(2)} ميجا
     `);
    }
    this.checkAttachments();
  }

  clearFile(i) {
    this.attachments[i].url = null;
    this.attachments[i].src = null;
    this.checkAttachments();
  }


  checkAttachments() {
    this.attachmentsInvalid = this.attachments.some(el => el.required ? el.src == null : el.name);
    this.attachmentList = [];
    this.attachments.map(el => {
      this.attachmentList.push({
        Id: el.id,
        Src: el.src
      });
    });
  }


}
