import { SessionAttendanceStatuses } from './../../../shared/enum/sessions.enum';
import { ActivatedRoute } from '@angular/router';
import { HelperService } from './../../../shared/services/helper.service';
import { IResponse } from './../../../shared/interfaces/response.interface';
import { Component, OnInit, } from '@angular/core';
import { SessionsService } from 'app/shared/services/sessions.service';
import { Location } from '@angular/common';
import { SessionAttendanceTypes } from 'app/shared/enum/sessions.enum';

@Component({
  selector: 'app-previous-details',
  templateUrl: './previous-details.component.html',
  styleUrls: ['./previous-details.component.scss']
})
export class PreviousDetailsComponent implements OnInit {
  students = [];
  session
  totalCash=0;
  paidCashIn=0;
  paidCashOut=0;
  
  studentsFiltered = [];
  studentsFilteredName = [];
  SessionAttendanceTypes = SessionAttendanceTypes
  SessionAttendanceStatuses = SessionAttendanceStatuses
  public columns = [
    { name: "Name", prop: "name" },
  ];
  constructor(
    private sessionService: SessionsService,
    private helper: HelperService,
    private location:Location,
    private route:ActivatedRoute
  ) { }

  ngOnInit(): void {
    if(this.route.snapshot.params.id) {
      this.getAll();
    }else{
      this.location.back()
    }
  }


  async getAll() {
    this.students = [];
    this.studentsFiltered = [];
    try {
      const res: IResponse = await this.sessionService.getSessionAttendance({ sessionId: +this.route.snapshot.params.id });
      if (res && res.success) {
        this.students = res.returnObject.attendance;
        this.session = res.returnObject.session
        this.studentsFiltered = this.students;
        this.studentsFilteredName = this.studentsFiltered;
        this.totalCash = res.returnObject.total;
          this.paidCashIn = res.returnObject.paid;
          this.paidCashOut = res.returnObject.paidCash;
      } else {
        this.helper.alertError(res.arabicMessage);
      }
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }

  }

  search(event) {
    this.studentsFiltered = this.students.filter(value => value.studentDisplayName.toLowerCase().includes(event.target.value));
    this.studentsFilteredName = this.studentsFiltered;
  }

  searchMobile(event) {
    this.studentsFiltered = this.studentsFilteredName.filter(value => value.studentPhoneNumber.toLowerCase().includes(event.target.value));
  }


}

