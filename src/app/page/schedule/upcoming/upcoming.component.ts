import { HelperService } from './../../../shared/services/helper.service';
import { IResponse } from './../../../shared/interfaces/response.interface';
import { SessionsService } from './../../../shared/services/sessions.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.scss']
})
export class UpcomingComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('tableResponsive') tableResponsive: any;
  sessions
  subjects
  totalCount
  TotalCash=0;
  CashIn=0;
  CashOut=0;
  pager: IPageInfo;
  public ColumnMode = ColumnMode;
  public limitRef = 10;
  public columns = [
    { name: "Name", prop: "name" },
  ];

  body = {
    SubjectId: '',
    From: '',
    To: '',
  }
  constructor(
    private modalService: NgbModal,
    private helper: HelperService,
    private sessionService: SessionsService,
  ) {
    moment.locale('ar_SA')
  }
  moment = moment;

  ngOnInit(): void {
    this.pager = {
      page: 1,
      pages: 3,
      pageSize: 10,
      total: 30,
    };
    this.getSubjects();
    this.getAll();
  }
  /**
   * upendDateLimit
   *
   * @param limit
   */
  upendDateLimit(limit) {
    this.pager.pageSize = limit.target.value;
    this.getAll();
  }



  setPage(event) {
    this.pager.page = event.offset + 1; //offset  of  ngx-datatable  = 0 at first page
    this.getAll();
  }

  async getAll() {
    this.sessions = [];
    try {
      const res: IResponse = await this.sessionService.getTeacherComingSessions(this.body);
      if (res && res.success) {
        this.sessions = res.returnObject.sessions;
        this.totalCount = res.returnObject.totalCount;
        this.TotalCash = res.returnObject.totalCash;
        this.CashIn = res.returnObject.cashIn;
        this.CashOut = res.returnObject.cashOut;
      } else {
        this.helper.alertError(res.arabicMessage);
      }
      this.modalService.dismissAll()
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }

  async getSubjects() {
    this.subjects = [];
    try {
      const res: IResponse = await this.sessionService.getMySubjects();
      if (res && res.success) {
        this.subjects = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }

  }


  reset() {
    this.body = {
      SubjectId: '',
      From: '',
      To: '',
    }
    this.getAll();
  }

  openModal(modalName, size ,backdrop?) {
    this.modalService.open(
      modalName,
      {
        windowClass: 'modal-holder ',
        backdropClass: 'light-blue-backdrop',
        centered: true, keyboard: false,
        backdrop: backdrop ? 'static' : null,
        size: size
      });
  }
}
interface IPageInfo {
  page: number;
  pageSize?: number;
  total: number;
  pages: number;
}
