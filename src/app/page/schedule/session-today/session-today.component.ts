import { HelperService } from './../../../shared/services/helper.service';
import { IResponse } from './../../../shared/interfaces/response.interface';
import { SessionsService } from './../../../shared/services/sessions.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-session-today',
  templateUrl: './session-today.component.html',
  styleUrls: ['./session-today.component.scss']
})
export class SessionTodayComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('tableResponsive') tableResponsive: any;
  sessions
  subjects
  totalCount
  totalCash=0;
  totalCashIn=0;
  totalCashOut=0;
  pager: IPageInfo;
  public ColumnMode = ColumnMode;
  public limitRef = 10;
  public columns = [
    { name: "Name", prop: "name" },
  ];
   today = new Date();
   date = this.today.getFullYear()+'-'+(this.today.getMonth()+1)+'-'+this.today.getDate();

  body = {
    SubjectId: '',
    From:this.date +'T00:00',
    To: this.date +'T23:59',
  }
  constructor(
    private modalService: NgbModal,
    private helper: HelperService,
    private sessionService: SessionsService,
  ) {
    moment.locale('en')
  }
  moment = moment;

  ngOnInit(): void {
    this.pager = {
      page: 1,
      pages: 3,
      pageSize: 10,
      total: 30,
    };
    this.getSubjects();
    this.getAll();
  }
  /**
   * upendDateLimit
   *
   * @param limit
   */
  upendDateLimit(limit) {
    this.pager.pageSize = limit.target.value;
    this.getAll();
  }



  setPage(event) {
    this.pager.page = event.offset + 1; //offset  of  ngx-datatable  = 0 at first page
    this.getAll();
  }

  async getAll() {
    this.sessions = [];
    try {
      const res: IResponse = await this.sessionService.getTeacherComingSessions(this.body);
      if (res && res.success) {
        this.sessions = res.returnObject.sessions;
        this.totalCount = res.returnObject.totalCount;
        this.totalCash = res.returnObject.totalCash;
        this.totalCashIn = res.returnObject.cashIn;
        this.totalCashOut = res.returnObject.cashOut;
      } else {
        this.helper.alertError(res.arabicMessage);
      }
      this.modalService.dismissAll()
    } catch (err) {
      if(err.status != 401)
      {
        console.log(err);
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
      }
      
    }
  }

  async getSubjects() {
    this.subjects = [];
    try {
      const res: IResponse = await this.sessionService.getMySubjects();
      if (res && res.success) {
        this.subjects = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      if(err.status != 401)
      {
        console.log(err);
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
      }
        
    }

  }

  openModal(modalName, size ,backdrop?) {
    this.modalService.open(
      modalName,
      {
        windowClass: 'modal-holder ',
        backdropClass: 'light-blue-backdrop',
        centered: true, keyboard: false,
        backdrop: backdrop ? 'static' : null,
        size: size
      });
  }
}
interface IPageInfo {
  page: number;
  pageSize?: number;
  total: number;
  pages: number;
}
