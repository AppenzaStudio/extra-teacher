import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionTodayComponent } from './session-today.component';

describe('SessionTodayComponent', () => {
  let component: SessionTodayComponent;
  let fixture: ComponentFixture<SessionTodayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionTodayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionTodayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
