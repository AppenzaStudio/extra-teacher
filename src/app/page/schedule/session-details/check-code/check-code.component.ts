import { Component, EventEmitter, OnInit, Output, ViewChild, Input } from '@angular/core';

@Component({
  selector: 'app-check-code',
  templateUrl: './check-code.component.html',
  styleUrls: ['./check-code.component.scss']
})
export class CheckCodeComponent implements OnInit {
  @ViewChild("input1") input1;
  @ViewChild("input2") input2;
  @ViewChild("input3") input3;
  @ViewChild("input4") input4;
  @ViewChild("input5") input5;
  code1;
  code2;
  code3;
  code4;
  code5;

  @Input() founded = false;
  @Output() code = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.input1.nativeElement.focus();
    }, 500);
  }
  ngOnChanges(): void {
    console.log('this.founded', this.founded);
    if (this.founded ) {
      this.clearAll();
    }

  }

  clearAll() {
    this.code1 = '';
    this.code2 = '';
    this.code3 = '';
    this.code4 = '';
    this.code5 = '';
    this.input1.nativeElement.focus();
    this.founded = false;
  }
  changeFocus(num, event) {
    const key = event.keyCode || event.charCode;
    if (num == 1 && this.code1) {
      this.input2.nativeElement.focus();
    }
    if (num == 2 && key == 8 || key == 46 ) {
      this.input1.nativeElement.focus();
    } else if ( this.code2) {
      this.input3.nativeElement.focus();
    }

    if (num == 3 && key == 8 || key == 46) {
      this.input2.nativeElement.focus();
    } else if ( this.code3) {
      this.input4.nativeElement.focus();
    }

    if (num == 4 && key == 8 || key == 46) {
      this.input3.nativeElement.focus();
    } else if (this.code4) {
      this.input5.nativeElement.focus();
    }
    if (num == 5 && key == 8 || key == 46) {
      this.input4.nativeElement.focus();
    } else if (this.code5) {
      this.code.emit(this.code1 + this.code2 + this.code3 + this.code4 + this.code5 + '');
    }
  }
}
