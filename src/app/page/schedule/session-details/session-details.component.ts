import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SessionAttendanceStatuses, SessionAttendanceTypes } from './../../../shared/enum/sessions.enum';
import { ActivatedRoute } from '@angular/router';
import { HelperService } from './../../../shared/services/helper.service';
import { IResponse } from './../../../shared/interfaces/response.interface';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SessionsService } from 'app/shared/services/sessions.service';
import { Location } from '@angular/common';
import Swal from "sweetalert2";

@Component({
  selector: 'app-session-details',
  templateUrl: './session-details.component.html',
  styleUrls: ['./session-details.component.scss']
})
export class SessionDetailsComponent implements OnInit {
  @ViewChild('close') close;
  students = [];
  totalCash=0;
  paidCashIn=0;
  paidCashOut=0;

  session;
  studentsFiltered = [];
  studentsOnline = [];
  studentsOnlineAttended = [];
  studentsFilteredName = [];
  selected = [];
  studentName = '';
  isFounded = false;
  isAllChecked = false;
  SessionAttendanceTypes = SessionAttendanceTypes;
  SessionAttendanceStatuses = SessionAttendanceStatuses;
  public columns = [
    { name: "Name", prop: "name" },
  ];
  studentCode;
  submitted = false;
  phoneVaild = true;

  body = {
    sessionId: 0,
    studentCode: null,
    studentDisplayName: null,
    studentPhoneNumber: null,
    studentId: 0,
  };
  constructor(
    private sessionService: SessionsService,
    private helper: HelperService,
    private modalService: NgbModal,
    private location: Location,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {

    if (this.route.snapshot.params.id) {
      this.getAll();

    } else {
      this.location.back();
    }
  }
  showConfirmation() {
    Swal.fire({
      title: 'تاكيد',
      text: `هل تريد تسجيل الغياب ل  ${this.selected.length} طلاب؟`,
      //text: "You won't be able to revert this!",
      icon: "info",
      showCancelButton: true,
      confirmButtonColor: "#2fa062",
      cancelButtonColor: "#d33",
      confirmButtonText: 'تاكيد',
      cancelButtonText: 'إلغاء',
    }).then((result) => {
      if (result.value) {
        this.takeAbsence();
      }
    });
  }
  checkPhone() {
    let re = new RegExp('^1[0-2,5]{1}[0-9]{8}$');
    this.phoneVaild = re.test(this.body.studentPhoneNumber);
  }


  async getAll() {
    this.students = [];
    this.studentsFiltered = [];
    this.studentsOnline = [];
    try {
      const res: IResponse = await this.sessionService.getSessionAttendance({ sessionId: +this.route.snapshot.params.id });
      if (res && res.success) {
        this.students = res.returnObject.attendance;
        if (this.students?.length > 0) {
          this.students.map(el => {
            if (el.status == this.SessionAttendanceStatuses.Attended)
              el.checked = true;

              if(el.type == this.SessionAttendanceTypes.Online){
                this.studentsOnline.push(el)
              }
          });
          this.session = res.returnObject.session;
          this.studentsFiltered = this.students;
          this.studentsFilteredName = this.studentsFiltered;
          this.totalCash = res.returnObject.total;
          this.paidCashIn = res.returnObject.paid;
          this.paidCashOut = res.returnObject.paidCash;
          this.studentsOnlineAttended = this.studentsOnline.filter(x=>x.status == this.SessionAttendanceStatuses.Attended)
        }

      } else {
        this.helper.alertError(res.arabicMessage);
      }
    } catch (err) {
      if (err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }

  }

  search(event) {
    this.studentsFiltered = this.students.filter(value => value.studentDisplayName.toLowerCase().includes(event.target.value));
    this.studentsFilteredName = this.studentsFiltered;
  }

  searchMobile(event) {
    this.studentsFiltered = this.studentsFilteredName.filter(value => value.studentPhoneNumber.toLowerCase().includes(event.target.value));
  }


  select(event, index) {
    this.studentsFiltered[index].checked = event.target.checked;
    this.selected = this.studentsFiltered.filter(el => el.checked);
    this.isAllChecked = this.studentsFiltered.some(el => !el.checked) ? false : true;
  }

  checkAll(event) {
    this.isAllChecked = event.target.checked;
    this.studentsFiltered.map(el => el.checked = event.target.checked);
    this.selected = this.studentsFiltered.filter(el => el.checked);
  }

  checkCode(code) {
    this.submitted = true;
    this.studentCode = code;
    let index = this.students.findIndex(el => { console.log(el.confirmationCode, this.studentCode + ''); return el.confirmationCode == this.studentCode + ''; });
    if (index >= 0) {
      this.isFounded = true;
      this.studentsFiltered = [];
      this.students[index].checked = true;
      this.studentName = this.students[index].studentDisplayName;
      this.studentsFiltered = this.students;
      this.selected = this.studentsFiltered.filter(el => el.checked && el.type == this.SessionAttendanceTypes.Online);
      this.studentCode = '';
      console.log('this.selected', this.selected);
    } else {
      this.isFounded = false;
    }
  }


  async takeAbsence() {
    try {
      let sessionAttandanceIds = [];
      this.selected.map(el => {
        sessionAttandanceIds.push(el.id);
      });
      let body = {
        sessionId: +this.route.snapshot.params.id,
        sessionAttandanceIds
      };
      const res: IResponse = await this.sessionService.takeStudentsAbsence(body);
      if (res && res.success) {

        Swal.fire({
          title: 'تم تسجيل الغياب بنجاح',
          //text: "You won't be able to revert this!",
          icon: "success",
        });
        this.getAll();
        this.isAllChecked = false;
        this.isFounded = false;
        this.submitted = false;

        this.modalService.dismissAll();
      } else {
        console.log('err0r');
        this.helper.alertError(res.arabicMessage);
      }
    } catch (err) {
      if (err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }

  }

  openModal(modalName, size, backdrop?) {
    this.selected = this.studentsFiltered.filter(el => el.checked && el.type == this.SessionAttendanceTypes.Online);
    console.log(this.selected)
    this.modalService.open(
      modalName,
      {
        windowClass: 'modal-holder ',
        backdropClass: 'light-blue-backdrop',
        centered: true, keyboard: false,
        backdrop: backdrop ? 'static' : null,
        size: size
      });
  }
  async attendOffline() {
    try {
      let studentsIds = [];
      this.selected.map(el => {
        studentsIds.push(el.id);
      });
      this.body.sessionId = +this.route.snapshot.params.id;
      this.body.studentPhoneNumber = '0' + this.body.studentPhoneNumber;
      const res: IResponse = await this.sessionService.attendOfflineStudent(this.body);
      if (res && res.success) {
        this.modalService.dismissAll();
        this.helper.successMsg(res.arabicMessage);
        this.getAll();
        this.isAllChecked = false;
        this.body = {
          sessionId: 0,
          studentCode: null,
          studentDisplayName: null,
          studentPhoneNumber: null,
          studentId: 0,
        };
      } else {
        this.helper.alertError(res.arabicMessage);
      }
    } catch (err) {
      if (err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }

  }
}

