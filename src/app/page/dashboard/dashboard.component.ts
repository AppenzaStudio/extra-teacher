import { Component, OnInit } from '@angular/core';
import { DashboardServiceService } from 'app/shared/services/dashboardServices/dashboard-service.service';
import { HelperService } from 'app/shared/services/helper.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  status:any;

  constructor(private dashboardServiceService : DashboardServiceService,
    private helper : HelperService) { }

  ngOnInit(): void {
    this.getStatus();
  }

  async getStatus() {
    try {
      const res: any = await this.dashboardServiceService.GetHomeStatus();
      if (res.statusCode.toString().startsWith('2')) {
        this.status = res.returnObject
      } else {
        this.helper.alertError(res.arabicMessage);
      }
    } catch (err) {
      this.helper.alertError(err.error.title);
      if (err.error.errors) {
        for (let key in err.error.errors) {
          this.helper.alertError(err.error.errors[key], key);
        }
      }
    }
  }
  

}
