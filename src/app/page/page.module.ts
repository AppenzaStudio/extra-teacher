import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { PageRoutingModule } from "./page-routing.module";
import { WebcamModule } from 'ngx-webcam';

import { PageComponent } from "./page.component";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { LottieModule } from 'ngx-lottie';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { ProfileComponent } from './profile/profile.component';
import { RevenueComponent } from './revenue/revenue.component';
import { UpcomingComponent } from './schedule/upcoming/upcoming.component';
import { PreviousComponent } from './schedule/previous/previous.component';
import { SessionDetailsComponent } from './schedule/session-details/session-details.component';

import { MatExpansionModule } from '@angular/material/expansion';
import { RequestsComponent } from './requests/requests.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { NgSelectModule } from '@ng-select/ng-select';
import { ToastrModule } from 'ngx-toastr';
import { PreviousDetailsComponent } from './schedule/previous-details/previous-details.component';
import { ApproviedGuardService } from 'app/shared/services/userServices/approved-guard.service';
import { SessionTodayComponent } from './schedule/session-today/session-today.component';
import { CheckCodeComponent } from './schedule/session-details/check-code/check-code.component';

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    LottieModule,
    FormsModule,
    WebcamModule,
    MatExpansionModule,
    PageRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxSpinnerModule,
    MatTabsModule,
    NgSelectModule,
    ToastrModule.forRoot({
      progressBar: true,
      progressAnimation: 'decreasing',
    }),
  ],
  exports: [],
  declarations: [
    PageComponent,
    DashboardComponent,
    ScheduleComponent,
    ProfileComponent,
    RevenueComponent,
    UpcomingComponent,
    PreviousComponent,
    SessionDetailsComponent,
    RequestsComponent,
    PreviousDetailsComponent,
    SessionTodayComponent,
    CheckCodeComponent,

  ],
  providers: [
    ApproviedGuardService
  ],
})
export class PageModule { }
