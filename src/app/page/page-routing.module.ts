import { SessionTodayComponent } from './schedule/session-today/session-today.component';
import { PreviousDetailsComponent } from './schedule/previous-details/previous-details.component';
import { RequestsComponent } from './requests/requests.component';
import { SessionDetailsComponent } from './schedule/session-details/session-details.component';
import { UpcomingComponent } from './schedule/upcoming/upcoming.component';
import { PreviousComponent } from './schedule/previous/previous.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageComponent } from './page.component';
import { ApproviedGuardService } from 'app/shared/services/userServices/approved-guard.service';


const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    data: {
      title: 'Page'
    }
    // children: [

    // ]
  },
  {
    path:'dashboard',
    // canActivate:[ApproviedGuardService],
    component:DashboardComponent
  },
  {
    path:'requests',
    component:RequestsComponent
  },
  {
    path:'profile',
    component:ProfileComponent
  },
  {
    path:'session/upcoming',
    // canActivate:[ApproviedGuardService],

    component:UpcomingComponent
  },
  {
    path:'session/today',
    // canActivate:[ApproviedGuardService],

    component:SessionTodayComponent
  },
  {
    path:'session/previous',
    // canActivate:[ApproviedGuardService],

    component:PreviousComponent
  },
  {
    path:'session/previous/:id',
    // canActivate:[ApproviedGuardService],

    component:PreviousDetailsComponent
  },

  {
    path:'session/upcoming/:id',
    // canActivate:[ApproviedGuardService],

    component:SessionDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageRoutingModule { }
