import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HelperService } from 'app/shared/services/helper.service';
import { AuthUserService } from 'app/shared/services/userServices/auth-user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {


  userForm!: FormGroup;
  userFormSubmitted = false;
  coverImageUrl:string;


  constructor(private fb: FormBuilder,
    public auth: AuthUserService,
    private helper: HelperService) { }

  ngOnInit(): void {
    this.buildForm();
    this.getUser();
  }

  buildForm(): void {
    this.userForm = this.fb.group({
      email: ['', Validators.email],
      fullName: ['', Validators.required],
      displayName: ['', Validators.required],
      phoneNumber: [null, [
        Validators.required,
        Validators.pattern(/^1[0-2,5]{1}[0-9]{8}$/),
      ]],
    });
  }

  get f() {
    return this.userForm.controls;
  }

  async getUser() {
    try {
      const res: any = await this.auth.getProfile();
      if (res.statusCode.toString().startsWith('2')) {
        let user = res.returnObject
        this.userForm.patchValue(user);
        this.userForm.get('email').setValue(user.email);
        this.userForm.get('fullName').setValue(user.fullName);
        this.userForm.get('displayName').setValue(user.displayName);
        this.userForm.get('phoneNumber').setValue('0'+user.phoneNumber);
        this.coverImageUrl = user.coverImageUrl;
        this.userForm.updateValueAndValidity();
      } else {
        this.helper.errorMsg(res.arabicMessage);
      }
    } catch (err) {
      this.helper.errorMsg(err.error.title);
      if (err.error.errors) {
        for (let key in err.error.errors) {
          this.helper.errorMsg(err.error.errors[key], key);
        }
      }
    }
  }

  async updateUser() {
    try {
      this.userFormSubmitted = true;
      if (this.userForm.invalid) {
        return;
      }
      let body = {
        displayName: "",
        coverImageUrl: this.coverImageUrl,
        fullName: "",
        phoneNumber: null
      };
      body.fullName = this.userForm.get('fullName')?.value;
      body.displayName = this.userForm.get('displayName')?.value;
      body.phoneNumber = this.userForm.get('phoneNumber')?.value;
      const res: any = await this.auth.updateProfile(body);
      if (res.statusCode.toString().startsWith('2')) {
        this.helper.alertSuccess("تم تحديث البيانات بنجاح ");
      } else {
        this.helper.alertError(res.arabicMessage);
      }
    } catch (err) {
      this.helper.alertError(err.error.title);
      if (err.error.errors) {
        for (let key in err.error.errors) {
          this.helper.alertError(err.error.errors[key], key);
        }
      }
    }
  }

}
