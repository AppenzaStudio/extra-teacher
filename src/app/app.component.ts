import { ToastrService } from 'ngx-toastr';
import { ConnectionsService } from './shared/services/connection/connection.service';
import { Component, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { LoadingService } from './shared/services/loading/loading.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
    show =  false;
    subscription: Subscription;

    constructor(private router: Router,
      private toaster: ToastrService,
      private connect:ConnectionsService, private loading:LoadingService) {
    }

    ngOnInit() {

      this.connect.currentStatus.subscribe(value => {
        console.log('value',value);
        if(value === false){
          this.toaster.warning('انت في وضع عدم الاتصال');
        }
        if(value){
          this.toaster.success('  متصل الأن');
        }
      })
    this.loading.isLoading.subscribe(isLoading => {
        setTimeout(() => {
          this.show = isLoading;
        });
      });
        this.subscription = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd)
            )
            .subscribe(() => window.scrollTo(0, 0));
    }


    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }



}
