import { AuthUserService } from 'app/shared/services/userServices/auth-user.service';
import { Component, ViewChild, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';

// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../../../shared/directives/must-match.validator';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { IResponse } from 'app/shared/interfaces/response.interface';
import { LookupService } from 'app/shared/services/lookupServices/lookup.service';
import { HelperService } from 'app/shared/services/helper.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})

export class RegisterPageComponent implements OnInit {
  registerFormSubmitted = false;
  registerForm: FormGroup;
  getSchoolForm: FormGroup;
  tabs = ['First', 'Second', 'Third'];
  selected = new FormControl(0);


  uploadedFile: any;
  public fileData: any = null;
  fileChanged = false;
  urls = [];


  showPasswordText = false;
  genderList = [
    { name: 'ذكر', value: 1 },
    { name: 'أنثي', value: 2 },
  ];
  governorateList: any[] = [];
  districtList: any[] = [];
  schoolsList: any[] = [];
  subjectList: any[] = [];
  stageList: any[] = [];
  allGradeList: any[] = [];
  gradeList: any[] = [];

  completeProfile = false;
  teacherName: any;
  teacherEmail: any;
  attachments: any = [];
  attachmentList: any = [];
  attachmentsInvalid = true;
  SchoolForm = {
    govId: null,
    disId: null,
    schoolId: null,
    subjects: null,
  };

  constructor(private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private router: Router,
    private lookupService: LookupService,
    public auth: AuthUserService,
    private helper: HelperService
  ) { }

  ngOnInit(): void {
    let email = localStorage.getItem('email');
    let name = localStorage.getItem('fullName');
    let user_authenticated = localStorage.getItem('user_authenticated');
    if (user_authenticated == 'false') {
      this.completeProfile = true;
      this.teacherEmail = email;
      this.teacherName = name;
    }

    if (user_authenticated == 'true') {
      this.router.navigate(['/pages/login']);
    }
    this.buildForm();

    this.f.govId.valueChanges.subscribe(value => {
      this.f.disId.setValue(null);
    });
    this.f.disId.valueChanges.subscribe(value => {
      this.f.schoolId.setValue(null);
    });
    this.f.schoolId.valueChanges.subscribe(value => {
      this.f.stages.setValue(null);
    });

    this.f.stages.valueChanges.subscribe(value => {
      this.gradeList = [];
      let List = [];
      if (value) {
        value.map(item => {
          List.push(this.allGradeList.filter(el => el.stage == item)[0]);
        });
        List.map(el => {
          el.list.map(item => {
            this.gradeList.push(item);
          });
        });
      }
      this.f.grades.setValue(null);

    });

    this.f.grades.valueChanges.subscribe(value => {
      value ? this.getAllSubjects() : this.subjectList = [];
      this.f.subjects.setValue(null);
    });

  }

  get f() {
    return this.getSchoolForm.controls;
  }
  buildForm(): void {
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      nationalId: [null, [
        Validators.required,
        Validators.pattern(/^[0-9]{14}$/),
      ]],
      phoneNumber: [null, [
        Validators.required,
        Validators.pattern(/^1[0-2,5]{1}[0-9]{8}$/),
      ]],
      gender: [null, Validators.required],
      birthdate: [null, Validators.required],
      password: [null, [Validators.required, Validators.minLength(8),
      Validators.pattern(
        /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$-\/:-?{-~!"^_`\[\]@%$*])(?=.{8,})/
      )]],
      confirmPassword: [null, Validators.required],
    },
      {
        validators: this.checkPasswords
      }
    );
    this.getSchoolForm = this.fb.group({
      govId: [null, Validators.required],
      disId: [null, Validators.required],
      schoolId: [null, Validators.required],
      subjects: [null, Validators.required],
      stages: [null, Validators.required],
      grades: [null, Validators.required],
    });
    if (this.completeProfile) {
      this.registerForm.get('name').setValue(this.teacherName);
      this.registerForm.get('email').setValue(this.teacherEmail);
      this.registerForm.get('password').setValidators(null);
      this.registerForm.get('confirmPassword').setValidators(null);
      this.registerForm.updateValueAndValidity();
    }
  }

  get rf() {
    return this.registerForm.controls;
  }

  checkPasswords(control: AbstractControl) {
    // here we have the 'passwords' group
    const password = control.get('password').value;
    const confirmPassword = control.get('confirmPassword').value;
    const confirmPasswordErrors = control.get('confirmPassword').errors;

    if (confirmPasswordErrors && !confirmPasswordErrors.notSame) return;
    else if (password !== confirmPassword) {
      control.get('confirmPassword').setErrors({ notSame: true });
    } else {
      control.get('confirmPassword').setErrors(null);
    }
  }

  getIndex(index) {
    if (index == 2 && this.attachments.length == 0) {
      this.getAttachmentTypes();
    }
    if (index == 1 && (this.governorateList.length == 0 || this.subjectList.length == 0)) {
      this.getAllGovernorates();
      this.getAllStages();
    }
  }
  // upload
  uploadFile($event, index, size?) {

    this.fileData = $event.target.files[0];
    if ((this.fileData.size / 1024) <= size) {
      this.fileChanged = true;
      this.uploadedFile = true;
      this.attachments[index].url = this.sanitizer.bypassSecurityTrustUrl(
        URL.createObjectURL(this.fileData)
      );
      this.attachments[index].src = this.fileData;
    } else {
      this.helper.alertError(`
     هذا الملف يجيب ان يكون اقل من
     ${(size / 1024).toFixed(2)} ميجا
     `);
    }
    this.checkAttachments();
  }

  clearFile(i) {
    this.attachments[i].url = null;
    this.attachments[i].src = null;
    this.checkAttachments();
  }


  checkAttachments() {
    this.attachmentsInvalid = this.attachments.some(el => el.required ? el.src == null : el.name);
    this.attachmentList = [];
    this.attachments.map(el => {
      this.attachmentList.push({
        Id: el.id,
        Src: el.src
      });
    });
  }


  //  On submit click, reset field value
  async onSubmit() {
    try {
      this.registerFormSubmitted = true;
      if (this.registerForm.invalid) {
        return;
      }
      let body = {
        Email: this.registerForm.get('email')?.value,
        FullName: this.registerForm.get('name')?.value,
        PhoneNumber: this.registerForm.get('phoneNumber')?.value,
        Password: this.registerForm.get('password')?.value,
        DisplayName: this.registerForm.get('name')?.value,
        NationalId: this.registerForm.get('nationalId')?.value,
        Gender: this.registerForm.get('gender')?.value,
        BirthDate: this.registerForm.get('birthdate')?.value,
        SchoolId: this.getSchoolForm.get('schoolId')?.value,
        Subjects: this.getSchoolForm.get('subjects')?.value,
        Grades: this.getSchoolForm.get('grades')?.value
      };
      let formData = new FormData();
      formData = this.helper.jsonToFormData(body);
      for (let index = 0; index < this.attachmentList.length; index++) {
        formData.append(`Attachements[${index}].Id`, this.attachmentList[index].Id);
        formData.append(`Attachements[${index}].Src`, this.attachmentList[index].Src);
      }
      if (this.completeProfile) {
        const res: IResponse = await this.auth.completeProfile(formData);
        if (res && res.success) {
          this.helper.alertSuccess(res.arabicMessage);
          localStorage.clear();
          this.auth.saveUserData(res.returnObject, true);
          this.router.navigateByUrl('/page/session/today');
          // if(this.auth.hasApprovedApplications())
          //   this.router.navigateByUrl('/page/session/today');
          // else
          //   this.router.navigateByUrl('/page/requests');
        } else {
          this.helper.alertError(res.arabicMessage);
        }

      } else {

        const res: IResponse = await this.auth.register(formData);
        if (res && res.success) {
          this.helper.alertSuccess(res.arabicMessage);
          this.router.navigate(['/pages/login']);
        } else {
          this.helper.alertError(res.arabicMessage);
        }
      }
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }
  showPassword() {
    this.showPasswordText = !this.showPasswordText;
  }
  async getAllGovernorates() {
    try {
      const res: IResponse = await this.lookupService.getAllGovernorates();
      if (res && res.success) {
        this.governorateList = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }
  async getAlldistricts() {
    this.districtList = [];
    try {
      const res: IResponse = await this.lookupService.getDistrictsByGovId(+this.getSchoolForm.get('govId').value);
      if (res && res.success) {
        this.districtList = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }

  async getAttachmentTypes() {
    this.districtList = [];
    try {
      const res: IResponse = await this.lookupService.getRegistrationAttachments();
      if (res && res.success) {
        this.attachments = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }
  async getAllschools() {
    this.schoolsList = [];
    try {
      const res: IResponse = await this.lookupService.getSchoolsByDisId(+this.getSchoolForm.get('disId').value);
      if (res && res.success) {
        this.schoolsList = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }
  async getAllSubjects() {
    this.subjectList = [];
    try {
      const res: IResponse = await this.lookupService.getsubjectsbyGrades(this.f.grades?.value);
      if (res && res.success) {
        this.subjectList = res.returnObject;
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }

  async getAllStages() {
    this.stageList = [];
    this.allGradeList = [];
    try {
      const res: IResponse = await this.lookupService.getAllStagesWithGrades();
      if (res && res.success) {
        res.returnObject.map((stage) => {
          this.stageList.push({
            id: stage.id,
            name: stage.name,
          })
          this.allGradeList.push(
            {
              stage:stage.id,
              list:stage.grades
            }
          )
        })
      } else {
        this.helper.alertError(res.message);
      }
    } catch (err) {
      if(err.status != 401)
        this.helper.alertError("حدث خطأ ما برجاء المحاولة مرة اخرى");
    }
  }

}
