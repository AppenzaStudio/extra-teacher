import { IResponse } from './../../../shared/interfaces/response.interface';
import { Router } from "@angular/router";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from "@angular/forms"; import { ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Component, OnInit } from "@angular/core";
import { AuthUserService } from 'app/shared/services/userServices/auth-user.service';
import { HelperService } from 'app/shared/services/helper.service';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  APIKey;
  userId;
  canChange;
  userForm: FormGroup;
  same: boolean = false;
  constructor(
    private toaster: ToastrService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private auth: AuthUserService,
    private helper: HelperService,
  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.route.queryParams.subscribe((params) => {
      this.APIKey = params["ResetToken"];
      this.userId = params["UserId"];
      // if (this.APIKey) {
      //   this.sendApiKey();
      // }else{
      //   this.onLogin();
      // }
    });

  }

  buildForm(): void {
    this.userForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      confirmPassword: [null],
      apiKey: [null, [Validators.required]],
    });
  }

  formInit() {
    return {
      email: null,
      password: null,
      apiKey: null,
      confirmPassword: null
    };
  }
  resetData() {
    this.userForm.reset(this.formInit());
    this.userForm.updateValueAndValidity();
  }

  // async sendApiKey() {
  //   try {
  //     const res: IResponse = await this.auth.apiKey(this.APIKey);
  //     if (res.success) {
  //       this.helper.success("You can change password", "Change Password");
  //       this.canChange = true;
  //     } else {
  //       this.helper.error(res.message, "Change Password");
  //       this.canChange = false;
  //       this.onLogin();
  //     }
  //   } catch (error) {
  //     this.helper.error(error);
  //   }
  // }

  // On login link click
  onLogin() {
    this.router.navigateByUrl('/pages/login');
  }
  async submitPassword() {
    try {
      const body = {
        userId: this.userId,
        resetToken: this.APIKey,
        newPassword: this.userForm.get('password').value
      }
      const res: IResponse = await this.auth.apiKey(body);
      if (res.success) {
        this.helper.alertSuccess(res.arabicMessage);
        this.onLogin();
      } else {
        this.helper.alertError(res.message);
      }
    } catch (error) {
      this.helper.alertError(error);
    }
  }
  checkPasswords() {
    if (this.userForm.controls.password.value === this.userForm.controls.confirmPassword.value) {
      this.same = true;
    } else {
      this.same = false
    }

  }
}