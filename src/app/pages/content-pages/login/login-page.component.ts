import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
// import { AuthService } from 'app/shared/auth/auth.service';
import { HelperService } from 'app/shared/services/helper.service';
import { AuthUserService } from 'app/shared/services/userServices/auth-user.service';
import { environment } from 'environments/environment';
import firebase from 'firebase';
import { NgxSpinnerService } from "ngx-spinner";
import { LoginPage } from './loginpage/login-page';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent implements OnInit {

  loginFormSubmitted = false;
  isLoginFailed = false;

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    rememberMe: new FormControl(true)
  });
  showPasswordText = false;

  constructor(
    private router: Router,
    private helperService: HelperService,
    private loginPage: LoginPage,
    private authService: AuthUserService,
    private route: ActivatedRoute
    ) {
  }

  ngOnInit(): void {
    if (!firebase.apps.length) {
      firebase.initializeApp(environment.firebaseConfig);
    } else {
      firebase.app();
    }

    this.loginPage.OnPageEnter();
  }

  showPassword() {
    this.showPasswordText = !this.showPasswordText;
  }

  SSoLogin() {
    this.loginPage.MoESignin();
  }

  get lf() {
    return this.loginForm.controls;
  }
  // On submit button click
  async onSubmit() {
    try {
      this.loginFormSubmitted = true;
      if (this.loginForm.invalid) {
        return;
      }
      const res: any = await this.authService.signinUser(this.loginForm.value);
      if (res.success) {
        this.authService.saveUserData(res.returnObject, this.loginForm.get('rememberMe').value ? true : false);
        this.router.navigateByUrl('/page/session/today');
        // if(this.authService.hasApprovedApplications())
        //   this.router.navigateByUrl('/page/session/today');
        // else
        //   this.router.navigateByUrl('/page/requests');
      } else {
        this.helperService.alertError(res?.arabicMessage);
        this.isLoginFailed = true;
      }
    } catch (err) {
      this.isLoginFailed = true;
      this.helperService.errorMsg(err.error.title)
    }
  }

}
