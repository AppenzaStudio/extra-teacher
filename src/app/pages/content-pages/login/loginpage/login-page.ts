import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { HttpClient } from '@angular/common/http';
import { v4 as uuidv4 } from 'uuid';
import { AuthUserService } from '../../../../shared/services/userServices/auth-user.service';
import { Router } from '@angular/router';
import { HelperService } from 'app/shared/services/helper.service';
import { environment } from 'environments/environment';
import { EnvService } from '../../../../env.service';

@Injectable({
  providedIn: 'root',
})
export class LoginPage {
  db: any;
  private SSOWindow: any;

  constructor(
    private http: HttpClient,
    private authUserService: AuthUserService,
    private helperService: HelperService,
    private router: Router,
    private envService: EnvService
  ) { }

  OnPageEnter() {
    if (this.authUserService.isUserAuthenticated()) {
      this.router.navigateByUrl('/page/session/today');
      // if(this.authUserService.hasApprovedApplications())
      //   this.router.navigateByUrl('/page/session/today');
      // else
      //   this.router.navigateByUrl('/page/requests');
    }
  }

  MoESignin() {
    this.helperService.showSpinner();
    this.db = firebase.database();
    let guid = uuidv4();
    let url = `${this.envService.apiUrl + environment.SSOLogin}?userGuid=${guid}`;
    const SamlRequest = this.http.get<any>(`${url}`).toPromise();

    SamlRequest.then((result) => {
      if (result) {
        this.activateSSOfirebaselistener(guid);
        this.SSOWindow = window.open(result?.url, '_system');
      } else {
        this.helperService.alertError('حدث خطأ ما حاول مرة اخرى');
      }
    }).catch((error) => {
      this.helperService.alertError('حدث خطأ ما حاول مرة اخرى');
    });
  }

  activateSSOfirebaselistener(guid) {
    const tempStr = 'Extra Teacher Data';
    let userData;
    this.db.ref('sso/' + guid).set(tempStr);

    let userGuIdRef = this.db.ref('sso/' + guid);
    userGuIdRef.on('value', (snapshot) => {
      userData = snapshot.val();

      if (userData !== tempStr) {
        this.SSOWindow.close();
        userGuIdRef.off();
        userGuIdRef.remove();
        if (userData.Message === 'Not Found') {
          this.helperService.alertError('لم نتمكن من الوصول لحسابك');
          this.ssoLogout();
        } else {
          this.authUserService.saveUserFromFirebase(userData, true);
          if (userData.Token) {
            this.router.navigateByUrl('/page/session/today');
            // if(this.authUserService.hasApprovedApplications())
            //   this.router.navigateByUrl('/page/session/today');
            // else
            //   this.router.navigateByUrl('/page/requests');
          }
          else
            this.router.navigateByUrl('/pages/register');
        }
      }
    });
  }

  ssoLogout() {
    if (!firebase.apps.length) {
      firebase.initializeApp(environment.firebaseConfig);
    } else {
      firebase.app();
    }
    this.helperService.showSpinner();
    this.db = firebase.database();
    let guid = uuidv4();
    let url = `${this.envService.apiUrl + environment.SSOLogin}?userGuid=${guid}&logout=${true}`;
    const SamlRequest = this.http.get<any>(`${url}`).toPromise();

    SamlRequest.then((result) => {
      if (result) {
        this.activateSSOfirebaselistenerLogot(guid);
        this.SSOWindow = window.open(result?.url, '_system');
      } else {
        this.helperService.alertError('حدث خطأ ما حاول مرة اخرى');
      }
    }).catch((error) => {
      this.helperService.alertError('حدث خطأ ما حاول مرة اخرى');
    });
  }

  activateSSOfirebaselistenerLogot(guid) {
    const tempStr = 'Extra Teacher Data';
    let userData;
    this.db.ref('sso/' + guid).set(tempStr);

    let userGuIdRef = this.db.ref('sso/' + guid);
    userGuIdRef.on('value', (snapshot) => {
      userData = snapshot.val();

      if (userData !== tempStr) {
        console.log(userData);
        
        this.SSOWindow.close();
        userGuIdRef.off();
        userGuIdRef.remove();
        if (userData.Message === 'Logged out Successfully') {
          //this.auth.logout();
        } else {
          this.helperService.alertError('حدث خطأ أثناء تسجيل الخروج');
        }
      }
    });
  }
}
