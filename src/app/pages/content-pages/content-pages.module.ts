import { ForgotPasswordPageComponent } from './forgot-password/forgot-password-page.component';
import { RegisterPageComponent } from './register/register-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';

import { ContentPagesRoutingModule } from "./content-pages-routing.module";

import { ErrorPageComponent } from "./error/error-page.component";
import { LoginPageComponent } from "./login/login-page.component";

import { MatTabsModule } from '@angular/material/tabs';
import { NgSelectModule } from '@ng-select/ng-select';
import { ToastrModule } from 'ngx-toastr';
import { ResetPasswordComponent } from './reset-password/reset-password.component';


@NgModule({
    imports: [
        CommonModule,
        ContentPagesRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        NgxSpinnerModule,
        MatTabsModule,
        NgSelectModule,
        ToastrModule.forRoot({
            progressBar: true,
            progressAnimation: 'decreasing',
        }),
    ],
    declarations: [
        ErrorPageComponent,
        LoginPageComponent,
        ForgotPasswordPageComponent,
        RegisterPageComponent,
        ResetPasswordComponent,
    ]
})
export class ContentPagesModule { }
