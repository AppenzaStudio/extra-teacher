import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { HelperService } from 'app/shared/services/helper.service';
import { AuthUserService } from 'app/shared/services/userServices/auth-user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-forgot-password-page',
    templateUrl: './forgot-password-page.component.html',
    styleUrls: ['./forgot-password-page.component.scss']
})

export class ForgotPasswordPageComponent implements OnInit {

    @ViewChild('f') forogtPasswordForm: NgForm;

    userForm: FormGroup
    constructor(private router: Router,
        private fb: FormBuilder,
        private toaster: ToastrService,
        private auth: AuthUserService,
        private helper: HelperService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.buildForm()
    }

    // On submit click, reset form fields
    async onSubmit() {
        try {
            const res: any = await this.auth.resetPassword(this.userForm.value.email);
            if (res.success) {
                this.helper.alertSuccess(res.arabicMessage);
                localStorage.setItem('email', this.userForm.value.email)
            } else {
                this.helper.alertError(res.arabicMessage)
            }
        } catch (err) {
            this.helper.alertError(err)
        }
    }


    get f2() {
        return this.userForm.controls;
    }
    buildForm(): void {
        this.userForm = this.fb.group({
            email: [null, [Validators.required, Validators.email]]
        });
    }

    formInit() {
        return {
            email: null
        };
    }
    resetData() {
        this.userForm.reset(this.formInit());
        this.userForm.updateValueAndValidity();
    }


}
