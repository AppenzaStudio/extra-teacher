import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IResponse } from 'app/shared/interfaces/response.interface';
import { environment } from 'environments/environment';
import { EnvService } from '../../../env.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsServiceService {

  constructor(private http: HttpClient,private envService: EnvService) { }

  GetApplications() {
    let url = `${this.envService.apiUrl + environment.GetApplications}`;
    return this.http.get<IResponse>(`${url}`).toPromise();
  }

  AddApplications(body: any) {
    let url = `${this.envService.apiUrl + environment.AddApplication}`;
    return this.http.post<IResponse>(`${url}`, body).toPromise();
  }
}
