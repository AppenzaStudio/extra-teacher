import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IResponse } from 'app/shared/interfaces/response.interface';
import { environment } from 'environments/environment';
import { EnvService } from '../../../env.service';

@Injectable({
  providedIn: 'root'
})
export class LookupService {

  constructor(
    private http: HttpClient,private envService: EnvService
  ) { }

  getAllGovernorates() {
    let url = `${this.envService.apiUrl + environment.GetGovernorates}`;
    return this.http.get<IResponse>(`${url}`).toPromise();
  }
  getDistrictsByGovId(id) {
    let url = `${this.envService.apiUrl + environment.GetDistrictsByGovId}?govId=${id}`;
    return this.http.get<IResponse>(`${url}`).toPromise();
  }
  getSchoolsByDisId(id) {
    let url = `${this.envService.apiUrl + environment.GetSchoolsByDisId}?districtId=${id}`;
    return this.http.get<IResponse>(`${url}`).toPromise();
  }
  getRegistrationAttachments(){
    let url = `${this.envService.apiUrl + environment.GetRegistrationAttachments}`;
    return this.http.get<IResponse>(`${url}`).toPromise();
  }
  getApplicationAttachments(){
    let url = `${this.envService.apiUrl + environment.GetApplicationAttachments}`;
    return this.http.get<IResponse>(`${url}`).toPromise();
  }

  getAllSubjects(){
    let url = `${this.envService.apiUrl + environment.GetAllSubjects}`;
    return this.http.get<IResponse>(`${url}`).toPromise();
  }

  getAllStagesWithGrades(){
    let url = `${this.envService.apiUrl + environment.GetAllStagesWithGrades}`;
    return this.http.get<IResponse>(`${url}`).toPromise();
  }

  getsubjectsbyGrades(body){
    let url = `${this.envService.apiUrl + environment.GetsubjectsbyGrades}`;
    return this.http.post<IResponse>(`${url}`,body).toPromise();
  }
}
