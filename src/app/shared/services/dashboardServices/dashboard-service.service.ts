import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IResponse } from 'app/shared/interfaces/response.interface';
import { environment } from 'environments/environment';
import { EnvService } from '../../../env.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardServiceService {

  constructor(
    private http: HttpClient,private envService: EnvService
  ) { }

  GetHomeStatus() {
    let url = `${this.envService.apiUrl + environment.GetHomeStatus}`;
    return this.http.get<IResponse>(`${url}`).toPromise();
  }
}
