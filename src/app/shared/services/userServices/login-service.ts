import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { EnvService } from '../../../env.service';

@Injectable({
    providedIn: 'root',
})
export class LoginService {
    private BaseUrl;
    constructor(private http: HttpClient,private envService: EnvService) {
        this.BaseUrl = this.envService.apiUrl;
    }
}
