import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthUserService } from './auth-user.service';

@Injectable({
  providedIn: 'root',
})
export class ApproviedGuardService {
  constructor(private auth: AuthUserService, private router:Router) {}

  canActivate() {
    
    if (this.auth.hasApprovedApplications() ) {
      return true;
    } 
    this.router.navigateByUrl('/page/requests')
    return false;
  }
}
