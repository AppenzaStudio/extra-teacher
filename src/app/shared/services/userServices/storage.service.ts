import { Injectable } from '@angular/core';

type itemToBeStored = { key: string; value: unknown };

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor() {}

  get(key: string): string {
    return localStorage.getItem(key);
  }

  getNonPrimitive(key: string) {
    return JSON.parse(this.get(key));
  }

  set(key: string, value: unknown) {
    if (typeof value === 'object') {
      localStorage.setItem(key, JSON.stringify(value));
    } else {
      localStorage.setItem(key, value as string);
    }
  }

  setMultipleItems(items: itemToBeStored[]) {
    items.forEach((item) => {
      this.set(item.key, item.value);
    });
  }

  delete(key: string) {
    localStorage.removeItem(key);
  }

  /**
   * The values to be persisted should be primitives only
   */
  clear(keys?: string[]) {
    if (keys) {
      const unremovableItems = {};
      keys.forEach((key) => (unremovableItems[key] = this.get(key)));
      localStorage.clear();
      // tslint:disable-next-line:forin
      for (const key in unremovableItems) {
        this.set(key, unremovableItems[key]);
      }
    } else {
      localStorage.clear();
    }
  }
}
