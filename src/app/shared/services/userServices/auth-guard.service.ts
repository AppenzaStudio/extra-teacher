import { Injectable } from '@angular/core';
import { AuthUserService } from './auth-user.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService {
  constructor(private auth: AuthUserService) {}

  canActivate() {
    
    if (this.auth.isUserAuthenticated() ) {
      return true;
    }
    this.auth.logout();
    return false;
  }
}
