import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { IResponse } from 'app/shared/interfaces/response.interface';
import { environment } from 'environments/environment';
import { EnvService } from '../../../env.service';
import { StorageService } from './storage.service';

@Injectable({
    providedIn: 'root',
})
export class AuthUserService {
    private TOKEN_DATA_KEY = 'token';
    private USER_DATA_KEY = 'user_access_data';
    private USER_AUTH = 'user_authenticated';
    private USER_ROLE = 'User_role';

    constructor(
        private http: HttpClient,
        private storage: StorageService,
        private router: Router,
        private envService: EnvService
    ) { }

    // saveUserData(userDataKey) {
    //     this.storage.setMultipleItems([
    //         { key: this.USER_DATA_KEY, value: userDataKey },
    //         { key: this.TOKEN_DATA_KEY, value: userDataKey.Token },
    //         { key: this.USER_AUTH, value: 'true' },
    //     ]);
    // }
    saveUserData(obj, rememberMe) {
        if (rememberMe) {
            this.storage.set('token', obj?.token);
        }
        sessionStorage.setItem('token', obj?.token);
        this.storage.set('userId', obj?.id);
        this.storage.set('fullName', obj?.fullName);
        this.storage.set('email', obj?.email);
        this.storage.set('hasApprovedApplications', obj?.hasApprovedApplications);
        this.storage.set(this.USER_AUTH, true);
    }
    saveUserFromFirebase(obj, rememberMe) {
        if (rememberMe) {
            this.storage.set('token', obj?.Token);
        }
        sessionStorage.setItem('token', obj?.Token);
        this.storage.set('userId', obj?.Id);
        this.storage.set('fullName', obj?.FullName);
        this.storage.set('email', obj?.Email);
        this.storage.set('ssoAccount', true);
        this.storage.set(this.USER_AUTH, obj?.Token ? true : false);
        this.storage.set('hasApprovedApplications', obj?.HasApprovedApplications);
    }

    isUserAuthenticated(): boolean {
        return this.storage.get(this.USER_AUTH) == 'true';
    }
    
    hasApprovedApplications(): boolean {
        return this.storage.get('hasApprovedApplications') == 'true';
    }

    // getUserRole(): string {
    //     return this.storage.get(this.USER_ROLE);
    // }

    getTokenFromStorage(): string {
        return this.storage.get(this.TOKEN_DATA_KEY);
    }

    logout() {
        let language = this.storage.get('user-language');
        this.storage.clear();
        this.storage.set('user-language', language);
        this.router.navigateByUrl('/pages/login');
    }

    signinUser(body) {
        return this.http.post<IResponse>(`${this.envService.apiUrl + environment.LoginUser}`, body).toPromise();
    }

    register(body) {
        return this.http.post<IResponse>(`${this.envService.apiUrl + environment.Register}`, body).toPromise();
    }

    completeProfile(body) {
        return this.http.post<IResponse>(`${this.envService.apiUrl + environment.CompleteProfile}`, body).toPromise();
    }

    getProfile() {
        return this.http.get<IResponse>(`${this.envService.apiUrl + environment.GetProfile}`).toPromise();
    }
    updateProfile(body) {
        return this.http.post<IResponse>(`${this.envService.apiUrl + environment.UpdateProfile}`, body).toPromise();
    }
    resetPassword(mail) {
        return this.http.get<IResponse>(`${this.envService.apiUrl + environment.SendResetEmail}?email=${mail}`, {}).toPromise()
    }
    apiKey(body) {
        return this.http.post<IResponse>(`${this.envService.apiUrl + environment.ResetPassword}`, body).toPromise()
    }
    getMySubjects() {
        return this.http.get<IResponse>(`${this.envService.apiUrl + environment.GetMySubjects}`).toPromise();
    }


}
