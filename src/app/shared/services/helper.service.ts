
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import exportFromJSON from 'export-from-json';
import { TranslateService } from '@ngx-translate/core';
import * as XLSX from 'xlsx';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
@Injectable({
    providedIn: 'root'
})
export class HelperService {

    // public classficationLookUps: BehaviorSubject<any> = new BehaviorSubject(null);
    // public logisticLookUps: BehaviorSubject<any> = new BehaviorSubject(null);

    surveyData: BehaviorSubject<any> = new BehaviorSubject(null);

    constructor(private toastr: ToastrService,
        private spinner: NgxSpinnerService,
        private translate: TranslateService) { }

    errorMsg(msg: string, moduleName?: string) {
        this.toastr.error(msg, moduleName)
    }
    warningMsg(msg: string, moduleName?: string) {
        this.toastr.warning(msg, moduleName)
    }
    infoMsg(msg: string, moduleName?: string) {
        this.toastr.info(msg, moduleName)
    }
    successMsg(msg: string, moduleName?: string) {
        this.toastr.success(msg, moduleName)
    }

    clean(obj) {
        for (var propName in obj) {
            if (obj[propName] === null || obj[propName] === undefined) {
                delete obj[propName];
            }
        }
        return obj
    }

    buildFormData(formData, data, parentKey?) {
        if (
            data &&
            typeof data === 'object' &&
            !(data instanceof Date) &&
            !(data instanceof File)
        ) {
            Object.keys(data).forEach((key) => {
                this.buildFormData(
                    formData,
                    data[key],
                    parentKey ? `${parentKey}[${key}]` : key
                );
            });
        } else {
            const value = data == null ? '' : data;
            formData.append(parentKey, value);
        }
    }

    jsonToFormData(data) {
        const formData = new FormData();

        this.buildFormData(formData, data);

        return formData;
    }
    changeFormat(change) {
        const date = new Date(change);
        const dd = String(date.getDate()).padStart(2, '0');
        const mm = String(date.getMonth() + 1).padStart(2, '0');
        const yyyy = date.getFullYear();
        let hh = date.getHours();
        let m = date.getMinutes();
        const ampm = hh >= 12 ? 'PM' : 'AM';
        hh = hh % 12;
        hh = hh ? hh : 12; // the hour '0' should be '12'
        m = m < 10 ? 0 + m : m;

        return dd + '/' + mm + '/' + yyyy + ' ' + hh + ':' + m + ' ' + ampm;
    }
    export(dataJson, fileName, exportType?) {
        // if (!exportType)
        exportType = 'csv';
        const data = dataJson;
        exportFromJSON({ data, fileName, exportType });
    }
    getTranslation(wordYouNeedToTranslate: string): string {
        return this.translate.instant(wordYouNeedToTranslate);
    }

    exportArrayOfObjectsToExcel(data: any[], fileName?, sheetName?) {
        if (data?.length > 0) {


            let worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
            /*    worksheet["!cols"] = wscolMapped;*/
            const workbook: XLSX.WorkBook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(workbook, worksheet, sheetName);
            XLSX.writeFile(workbook, fileName + '.xlsx');
        }
        else {
            this.toastr.info('No Data To Download');
        }




    }

    confirmDelete(message: string, title = 'Confirmation') {
        return Swal.fire({
            title: title,
            text: message,
            icon: "error",
            showCancelButton: true,
            confirmButtonColor: "#48b0f7",
            cancelButtonColor: "#d33",
            confirmButtonText: `Ok`,
            cancelButtonText: 'Cancel',
        });
    }

    confirmAny(message: string, confirmButtonText = 'Delete') {
        return Swal.fire({
            title: 'Are you sure?',
            text: `${message}`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#2F8BE6',
            cancelButtonColor: '#F55252',
            confirmButtonText: confirmButtonText,
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger ml-1'
            },
            buttonsStyling: false,
        });
    }
    alertError(message, title = "خطأ!") {
        Swal.fire({
            title: title,
            showClass: {
                popup: 'animated tada'
            },
            text: `${message}`,
            icon: "error",
            customClass: {
                confirmButton: 'btn btn-primary'
            },
            buttonsStyling: false,
        });

    }

    alertSuccess(message) {
      Swal.fire({
          title: message,
          icon: "success",
          customClass: {
              confirmButton: 'btn btn-primary'
          },
          timer: 3000,
          buttonsStyling: false,
      });

  }

    showSpinner() {
        this.spinner.show(undefined,
            {
                type: 'ball-triangle-path',
                size: 'medium',
                bdColor: 'rgba(0, 0, 0, 0.8)',
                color: '#fff',
                fullScreen: true
            });
    }
    hideSpinner() {
        this.spinner.hide();
    }

}
