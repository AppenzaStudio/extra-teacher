import { environment } from './../../../environments/environment';
import { EnvService } from '../../env.service';
import { IResponse } from './../interfaces/response.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionsService {

  constructor(
    private http: HttpClient,
    private envService: EnvService
  ) { }


  getTeacherComingSessions(params){
    return this.http.get<IResponse>(`${this.envService.apiUrl + environment.GetTeacherComingSessions}`, {params}).toPromise();
  }
  getTeacherPreviousSessions(params){
    return this.http.get<IResponse>(`${this.envService.apiUrl + environment.GetTeacherPreviousSessions}`, {params}).toPromise();
  }
  getSessionAttendance(params){
    return this.http.get<IResponse>(`${this.envService.apiUrl + environment.GetSessionAttendance}`, {params}).toPromise();
  }
  takeStudentsAbsence(body){
    return this.http.post<IResponse>(`${this.envService.apiUrl + environment.TakeStudentsAbsence}`, body).toPromise();
  }
  attendOfflineStudent(body){
    return this.http.post<IResponse>(`${this.envService.apiUrl + environment.AttendOfflineStudent}`, body).toPromise();
  }

  getMySubjects(){
    return this.http.get<IResponse>(`${this.envService.apiUrl + environment.GetMySubjects}`).toPromise();

  }

}
