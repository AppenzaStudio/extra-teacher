import { ConnectionService } from 'ng-connection-service';
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConnectionsService {
  currentStatus: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(
    private connectionService: ConnectionService,
  ) {


    this.connectionService.monitor().subscribe(isConnected => {
      this.currentStatus.next(isConnected);
      console.log('isConnected', isConnected);
    });
  }


}
