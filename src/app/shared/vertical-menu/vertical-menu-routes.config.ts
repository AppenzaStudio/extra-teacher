import { RouteInfo } from './vertical-menu.metadata';
import { AuthUserService } from '../services/userServices/auth-user.service';
import { RoutesRecognized } from '@angular/router';
import { Injectable } from '@angular/core';

//Sidebar menu Routes and data
export const ROUTES: RouteInfo[] = [
  {
    path: '/page/dashboard', title: 'الرئيسية', icon: 'ft-home', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
  },
  {
    path: '/page/requests', title: 'الطلبات', icon: 'ft-layers', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
  },
  {
    path: '', title: 'الحصص', icon: 'ft-book', class: 'has-sub' , badge: '', badgeClass: '', isExternalLink: false, submenu: [
{ path: '/page/session/today', title: 'اليوم', icon: 'ft-arrow-right submenu-icon', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
{ path: '/page/session/upcoming', title: 'القادمة', icon: 'ft-arrow-right submenu-icon', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
      { path: '/page/session/previous', title: 'السابقة', icon: 'ft-arrow-right submenu-icon', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    ]
  },

];

@Injectable({
  providedIn: 'root',
})
export class GetRoutes{
  constructor(private authUserService:AuthUserService) {
  }
  Routes():RouteInfo[]{
    if(this.authUserService.hasApprovedApplications())
    {
     return [
        {
          path: '/page/dashboard', title: 'الرئيسية', icon: 'ft-home', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
        },
        {
          path: '/page/requests', title: 'الطلبات', icon: 'ft-layers', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
        },
        {
          path: '', title: 'الحصص', icon: 'ft-book', class: 'has-sub' , badge: '', badgeClass: '', isExternalLink: false, submenu: [
      { path: '/page/session/today', title: 'اليوم', icon: 'ft-arrow-right submenu-icon', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
      { path: '/page/session/upcoming', title: 'القادمة', icon: 'ft-arrow-right submenu-icon', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
            { path: '/page/session/previous', title: 'السابقة', icon: 'ft-arrow-right submenu-icon', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
          ]
        },

      ];
    }
    else{
     return [
        {
          path: '/page/requests', title: 'الطلبات', icon: 'ft-layers', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
        }
      ];
    }
  }
  HRoutes(){
    if(this.authUserService.hasApprovedApplications())
    {
     return [
        {
          path: '/page', title: 'Page', icon: 'ft-home', class: 'dropdown nav-item', isExternalLink: false, submenu: []
        },
        {
          path: '', title: 'Menu Levels', icon: 'ft-align-left', class: 'dropdown nav-item has-sub', badge: '', badgeClass: '', isExternalLink: false,
          submenu: [
              { path: '/YOUR-ROUTE-PATH', title: 'Second Level', icon: 'ft-arrow-right submenu-icon', class: 'dropdown-item', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
              {
                  path: '', title: 'Second Level Child', icon: 'ft-arrow-right submenu-icon', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
                  submenu: [
                      { path: '/YOUR-ROUTE-PATH', title: 'Third Level 1.1', icon: 'ft-arrow-right submenu-icon', class: 'dropdown-item', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
                      { path: '/YOUR-ROUTE-PATH', title: 'Third Level 1.2', icon: 'ft-arrow-right submenu-icon', class: 'dropdown-item', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
                  ]
              },
          ]
      },
      ];
    }
    else{
      return [
        {
          path: '', title: 'Menu Levels', icon: 'ft-align-left', class: 'dropdown nav-item has-sub', badge: '', badgeClass: '', isExternalLink: false,
        }
      ];
    }
  }
}
