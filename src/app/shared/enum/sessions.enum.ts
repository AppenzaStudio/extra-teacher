export enum SessionAttendanceTypes {
  Unknown = 0,
  Online = 1,
  Offline = 2
}


export enum SessionAttendanceStatuses {
  Unknown = 0,
  Pending = 1,
  Attended = 2,
  Absent = 3
}
