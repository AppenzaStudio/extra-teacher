export interface IResponse{
  success: boolean,
  message?: string,
  arabicMessage?: string,
  returnObject?: any,
  statusCode?: number
}

export interface IPageInfo
{
  page: number;
  pageSize?: number;
  total: number;
  pages: number;
}
