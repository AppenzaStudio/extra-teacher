import { LoadingService } from './../services/loading/loading.service';
import { catchError, finalize } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { EnvService } from '../../env.service';
import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { Router } from '@angular/router';
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  private token: string | null = null;

  constructor(private router: Router, private loadingService: LoadingService,private envService: EnvService) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.token = localStorage.getItem('token') ? localStorage.getItem('token') : sessionStorage.getItem('token');;

    // if (req.url.includes('/api/User/LoginUser')) {
    //   return next.handle(req);
    // }
    const authReq = req.clone({
      setHeaders: {
        Authorization: `Bearer ${this.token ? this.token : ''}`,
      }
    });

    if (authReq.url.includes(this.envService.apiUrl)) {
      this.loadingService.setLoading(true, authReq.url);
      return next.handle(authReq).pipe(
        catchError((error: HttpErrorResponse) => {
          if (error && error.status === 401) {
            // 401 errors are most likely going to be because we have an expired token that we need to refresh.
            localStorage.clear();
              this.router.navigateByUrl('/pages/login')


            return throwError(error);
          } else {
            return throwError(error);
          }
        }),
        finalize(() => this.loadingService.setLoading(false, authReq.url)));
    } else {
      return next.handle(authReq);
    }

  }
}
